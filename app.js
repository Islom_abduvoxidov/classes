class Momento {
  constructor() {
    this.momento = []
  }

  newMomento(name) {
    if (name !== 'undo') {
      this.momento.push(name)
      return name
    } else {
      this.momento.pop(name)
      return name
    }
  }
  get AllMomento() {
    return this.momento
  }
}

let momnet = new Momento()
momnet.newMomento('Nomoz')
momnet.newMomento('Aziz')
momnet.newMomento('Islom')
momnet.newMomento('undo')
momnet.newMomento('undo')
momnet.newMomento('Akbar') // output [ 'Nomoz', 'Akbar' ]

console.log(momnet.AllMomento)
